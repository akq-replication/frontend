var md5=require('md5');
var PouchDB=require('pouchdb');
var db=new PouchDB('todos');

// our very own request-promise-native (initially for JSON)
var request=function(url, method, body){
  return new Promise(function(resolve, reject){
    var httpRequest=new XMLHttpRequest();
    httpRequest.onload=function(){
	  resolve(JSON.parse(httpRequest.responseText));
	};
	httpRequest.onerror=function(){
	  reject(httpRequest.responseText); // TODO: improve: pass error object
	};
	httpRequest.onabort=function(){
	  reject(httpRequest.responseText); // TODO: improve: pass error object
	};
	httpRequest.open(method || "GET", url);
	if(body){
	  httpRequest.setRequestHeader("Content-type", "application/json");
	  httpRequest.send(JSON.stringify(body));
	}else{
      httpRequest.send();
	}
  });
};

// hand out to index.html b/c we don't have a framework (quick and dirty here)
// TODO: move all code here & register window.onload / other event handlers programmatically
window.db=db;
window.replicate=function() {
 return request("http://localhost:8088/aufgaben").then(function(allFromServer){
  var serverPromises=[];
  // 1.) Server 2 client
  allFromServer.forEach(function(docFromServer){
    console.log("Server 2 Client for " + docFromServer.uuid);
	serverPromises.push(db.get(docFromServer.uuid).then(function(docFromClient){
	  var clientTitleChanged=(docFromClient.title!=docFromClient.titleReplicated);
	  var serverTitleChanged=(docFromServer.title!=docFromClient.titleReplicated);
	  var anyChanged=(clientTitleChanged || serverTitleChanged || (docFromClient.done != (!!docFromServer.done)));
	  if(anyChanged){
		if(clientTitleChanged && serverTitleChanged){
			if(docFromClient.title!=docFromServer.title){
				docFromClient.titleConflict=docFromServer.title;
			}
		}else if(serverTitleChanged){
			docFromClient.title=docFromServer.title;
		}else if(clientTitleChanged){
			// see below
		}
		if(docFromClient.title==docFromServer.title){
			delete(docFromClient.titleConflict);
		}
		docFromClient.titleReplicated=docFromServer.title;
		docFromClient.done=(docFromClient.done || (!!docFromServer.done)); // one done = all done (heuristic instead of 3-way)
		return db.put(docFromClient).then(function(){
			console.log("Updated on Client: "+docFromClient._id);
		}); // cascade catch (just throw up the promise chain)
	  }
	  // (implicitly: return undefined meaning: successfully resolved)
	}).catch(function(err){
	  if("not_found"==err.name){
		return db.put({_id: docFromServer.uuid, title: docFromServer.title, titleReplicated: docFromServer.title, done: (!!docFromServer.done)}).then(function(){
			console.log("Created on Client: "+docFromServer.uuid);
		}); // cascade catch (just throw up the promise chain)
      }else{
	    throw err; // throw up, literally
	  }
	}));
  });
  // 2.) Client 2 Server
  var clientPromise=Promise.all(serverPromises).then(function(){
   return db.allDocs({include_docs: true}).then(function(res){
    var clientPromises=[];
    res.rows.forEach(function(docFromClient){
	  docFromClient=docFromClient.doc;
      console.log("Client 2 Server for "+docFromClient._id);
	  var docFromServer=allFromServer.filter(function(s){return s.uuid==docFromClient._id;})[0];
	  if(docFromServer){
	    var clientTitleChanged=(docFromClient.title!=docFromClient.titleReplicated);
		if(docFromClient.titleConflict){
			// don't push to server, as we have a conflict detected already
			console.log("Skip because of conflict");
		}else if(clientTitleChanged){
			  clientPromises.push(request("http://localhost:8088/aufgaben/"+docFromClient._id, "PUT", docFromClient).then(function(){
			  console.log("Updated on Server: "+docFromClient._id);
			  docFromClient.titleReplicated=docFromClient.title;
			  return db.put(docFromClient).then(function(){
				console.log("Remember replication: "+docFromClient._id);
			  }); // cascade catch
			})); // cascade catch
		}else{
		  // nothing
		}
	  }else{
	      clientPromises.push(request("http://localhost:8088/aufgaben/"+docFromClient._id, "PUT", docFromClient).then(function(){
		  console.log("Created on Server: "+docFromClient._id);
		  docFromClient.titleReplicated=docFromClient.title;
		  return db.put(docFromClient).then(function(){
			console.log("Remember replication: "+docFromClient._id);
		  }); // cascade catch
		})); // cascade catch
	  }
	});
	return Promise.all(clientPromises);
   });
  }); // cascade catch also
  // TODO: delete (later)
  return clientPromise;
 });
};